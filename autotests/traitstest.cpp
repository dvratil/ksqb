/*
 * SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "traits_p.h"

struct Table {};

int main()
{
    using Column = Qb::Column<Table, 0, int>;
    static_assert(Qb::traits::is_column_v<Column>);
    static_assert(Qb::traits::is_column_v<const Column>);
}
