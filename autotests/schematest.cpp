/*
 * SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "schema.h"
#include "qb.h"
#include "aggregates.h"

#include <iostream>
#include <tuple>


#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

using namespace std::string_view_literals;

struct ItemsTable : public Qb::Table<ItemsTable>
{
public:
    using IdColumn = Qb::Column<ItemsTable, 0, int>;
    using IsEnabledColumn = Qb::Column<ItemsTable, 1, bool>;
    using SizeColumn = Qb::Column<ItemsTable, 2, int>;

    static constexpr std::string_view name{"items"sv};

    static constexpr IdColumn id{"id"sv};
    static constexpr IsEnabledColumn isEnabled{"isEnabled"sv};
    static constexpr SizeColumn size{"size"sv};

    using AllColumns = std::tuple<IdColumn, IsEnabledColumn, SizeColumn>;
    static constexpr AllColumns allColumns = std::make_tuple(id, isEnabled, size);
};


struct Item : public Qb::Entity<Item, ItemsTable>
{
public:
    using IdValue = Qb::EntityValue<Item, ItemsTable::IdColumn>;
    using IsEnabledValue = Qb::EntityValue<Item, ItemsTable::IsEnabledColumn>;
    using SizeValue = Qb::EntityValue<Item, ItemsTable::SizeColumn>;

    IdValue id{*this};
    IsEnabledValue isEnabled{*this};
    SizeValue size{*this};
};


TEST_CASE("SELECT * FROM table WHERE ... ORDER BY")
{
    auto query = Qb::select(ItemsTable::allColumns)
                     .from<ItemsTable>()
                     .where((ItemsTable::id == 5) && (ItemsTable::isEnabled == false))
                     .orderBy(ItemsTable::id, Qb::Order::DESC)
                     .limit(10, 20);
    REQUIRE(query.toString() == "SELECT id, isEnabled "
                                "FROM items "
                                "WHERE ((id = 5) AND (isEnabled = 0)) "
                                "ORDER BY id DESC"
                                "LIMIT 10");

}

TEST_CASE("SELECT COUNT(*) AS cnt, size FROM table GROUP BY ...")
{
    auto query = Qb::select(Qb::Count{ItemsTable::id}.as("cnt"),
                            ItemsTable::size)
                    .from<ItemsTable>()
                    .where(ItemsTable::isEnabled == true)
                    .groupBy(ItemsTable::size)
                    .having(NamedColumn{"cnt"} > 10)
                    .orderBy(NamedColumn{"cnt"}, Qb::ASC);

    REQUIRE(query.toString() == "SELECT COUNT(id) AS cnt, size "
                                "FROM items "
                                "WHERE (isEnabled = 0) "
                                "GROUP BY size "
                                "HAVING cnt > 10 "
                                "ORDER BY cnt ASC");
}



