# KSQB - SQL Query Builder

KSQB is a C++17 header-only, template-based, type-safe SQL ORM framework and
query builder.

## ORM

The database schema can be either defined in C++ header, using macros and tools
provided by KSQB, or it can in an XML file (see below for details) that is converted
into C++ code at compile time using XSLT. KSQB provides CMake macros to easily
integrate this step into your build process.

## Query Builder

KSQB provides an SQL-like API functions that allows user to build SQL queries
using C++ API. Compared to other frameworks, KSQB offers more type-safety
by checking compatibility of arguments at compile time.

## API Examples

```
conn.select(
    Table::firstColumn,
    Table::secondColumn,
    Table::thirdColumn
).from<Table>().where(
    Table::firstColumn == 1 && Table::secondColumn == Table::thirdColumn
)

