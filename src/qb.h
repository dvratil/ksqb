/*
 * SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#pragma once

#include "select.h"

#include <tuple>

namespace Qb
{

template<typename ... Columns>
auto select(std::tuple<Columns...> &&columns)
{
    return SelectQuery<std::tuple<Columns ...>>{std::forward<std::tuple<Columns ...>>(columns)};
};

template<typename ... Columns>
auto select(Columns && ... columns)
{
    return SelectQuery<std::tuple<Columns ...>>{std::forward_as_tuple(columns ...)};
}

} // namespace Qb
