/*
 * SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#pragma once

#include "schema.h"

#include <type_traits>

namespace Qb
{

struct Distinct_t {};
static constexpr Distinct_t Distinct = {};

template<typename DistinctOrExpr, typename _Expr = void>
struct Count : public ResultColumn<Count<DistinctOrExpr, _Expr>>
{
public:
    using Expr = std::conditional_t<std::is_void_v<_Expr>, DistinctOrExpr, _Expr>;

    explicit Count(Expr &&expr)
        : expr(std::forward<Expr>(expr))
    {}

    static constexpr bool distinct = std::is_void_v<_Expr> ? false : true;

    Expr expr;
};

template<typename Expr> Count(Distinct_t &&, Expr &&expr) -> Count<Distinct_t, Expr>;
template<typename Expr> Count(Expr &&expr) -> Count<Expr>;



} // namespace Qb
