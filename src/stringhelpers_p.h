/*
 * SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#pragma once

#include "traits_p.h"
#include "condition.h"
#include "select.h"

#include <string_view>
#include <string>
#include <tuple>
#include <sstream>
#include <cassert>

namespace Qb
{

////////////////////////////////////////////////////////////////////
// Colum to string

template<typename _Column>
auto columnToString(const _Column &column)
    -> std::enable_if_t<traits::is_column_v<_Column>, std::string>
{
    if (column.alias.empty()) {
        return std::string{column.name};
    } else {
        return std::string{column.name} + " AS " + std::string{column.alias};
    }
}

template<typename T>
auto columnToString(const T &val)
    -> std::enable_if_t<!traits::is_column_v<T>, std::string>
{
    std::ostringstream str;
    str << val;
    return str.str();
}

///////////////////////////////////////////////////////////////////
// Columns tuple to string

// End of iteration
template<std::size_t I, typename ... Cols>
auto columnsToString(const std::tuple<Cols ...> &cols)
    -> std::enable_if_t<I == sizeof...(Cols), std::string>
{
    return {};
}

template<std::size_t I = 0, typename ... Cols>
auto columnsToString(const std::tuple<Cols ...> &cols)
    -> std::enable_if_t<(I < sizeof...(Cols)), std::string>
{
    std::string str;
    if constexpr (I > 0) {
        str += ", ";
    }

    return str + columnToString(std::get<I>(cols)) + columnsToString<I + 1>(cols);
}

/////////////////////////////////////////////////////////////////////
// Table to string

template<typename T>
std::string tableToString(const T &table)
{
    return std::string{T::name};
}

//////////////////////////////////////////////////////////////////////
// Condition to string

template<typename Cond>
std::string conditionToString(const Cond &c)
{
    if constexpr (Cond::relation == Relation::And) {
        return "(" + conditionToString(c.lhs) + " AND " + conditionToString(c.rhs) + ")";
    } else if constexpr (Cond::relation == Relation::Or) {
        return "(" + conditionToString(c.lhs) + " OR " + conditionToString(c.rhs) + ")";
    } else if constexpr (Cond::relation == Relation::Neg) {
        return "(NOT " + conditionToString(c.lhs) + ")";
    } else if constexpr (Cond::relation == Relation::Eq) {
        return "(" + columnToString(c.lhs) + " = " + columnToString(c.rhs) + ")";
    }

    assert(false);
    return {};
}

//////////////////////////////////////////////////////////////////////
// Order to string

template<std::size_t I, typename ... Orders>
auto ordersToString(const std::tuple<Orders ...> &orders)
    -> std::enable_if_t<I == sizeof...(Orders), std::string>
{
    return {};
}

template<std::size_t I = 0, typename ... Orders>
auto ordersToString(const std::tuple<Orders ...> &orders)
    -> std::enable_if_t<I < sizeof...(Orders), std::string>
{
    std::string str;
    if constexpr (I > 0) {
        str += ", ";
    }

    str += columnToString(std::get<I>(orders).column) + " ";
    if (std::get<I>(orders).order == Order::ASC) {
        str += "ASC";
    } else {
        str += "DESC";
    }

    return str;
}

//////////////////////////////////////////////////////////////////////
// Expr to string

template<typename Expr>
auto exprToString(const Expr &expr)
    -> std::enable_if_t<traits::is_stream_serializable_v<Expr>, std::string>
{
    std::stringstream str;
    str << expr;
    return str.str();
}

} // namespace Qb

