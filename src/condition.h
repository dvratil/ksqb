/*
 * SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#pragma once

#include "traits_p.h"

#include <type_traits>

namespace Qb
{

enum class Relation {
    And,
    Or,
    Neg,

    Eq
};


template<Relation Rel, typename Subcond1, typename Subcond2>
struct Condition
{
    using LHS = Subcond1;
    using RHS = Subcond2;
    static constexpr Relation relation = Rel;

    LHS lhs;
    RHS rhs;
};

template<typename Cond1, typename Cond2>
auto operator&&(const Cond1 &c1, const Cond2 &c2)
{
    return Qb::Condition<Relation::And, Cond1, Cond2>{c1, c2};
}

template<typename Cond1, typename Cond2>
auto operator||(const Cond1 &c1, const Cond2 &c2)
{
    return Qb::Condition<Relation::Or, Cond1, Cond2>{c1, c2};
}

template<typename Cond>
auto operator!(const Cond c)
{
    return Qb::Condition<Relation::Neg, Cond, void*>{c, nullptr};
}

} // namespace Qb

template<typename Column1, typename Column2,
         typename = std::enable_if_t<Qb::traits::is_column_v<Column1> && Qb::traits::is_column_v<Column2>>>
auto operator==(const Column1 &col1, const Column2 &col2)
{
    static_assert(std::is_same_v<Column1::type, Column2::type>, "Columns must have equal types.");
    return Qb::Condition<Qb::Relation::Eq, Column1, Column2>{col1, col2};
}

template<typename Column,
         typename = std::enable_if_t<Qb::traits::is_column_v<Column>>>
auto operator==(const Column &col, const typename Column::type &val)
{
    return Qb::Condition<Qb::Relation::Eq, Column, typename Column::type>{col, val};
}

