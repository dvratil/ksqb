/*
 * SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#pragma once

#include <string_view>
#include <utility>
#include <array>

namespace Qb
{

template<typename _Impl>
struct ResultColumn
{
public:
    using Impl = _Impl;

    Impl & as(std::string_view as)
    {
        alias = as;
        return static_cast<Impl &>(*this);
    }

    std::string_view alias{};
};


template<typename _Table,
         size_t _index,
         typename _Type>
struct Column : ResultColumn<Column<_Table, _index, _Type>>
{
public:
    enum class Feature {
        Null          = 0x01,
        AutoIncrement = 0x02,
        Primary       = 0x04
    };

    using table = _Table;
    using type = _Type;

    static constexpr size_t index = _index;

    const std::string_view name = {};
    const int features = 0;
    const type defaultValue = {};

    constexpr explicit Column(std::string_view name)
        : name(name)
    {}
    constexpr explicit Column(std::string_view name, int features)
        : name(name), features(features)
    {}
    constexpr explicit Column(std::string_view name, int features, type &&defaultValue)
        : name(name), features(features), defaultValue(std::move(defaultValue))
    {}
};

template<typename _EntityImpl, typename _Table>
struct Entity
{
    using table = _Table;
    using impl = _EntityImpl;

    std::array<bool, std::tuple_size<decltype(table::allColumns)>::value> changed;
};

template<typename _Entity, typename _Column>
struct EntityValue
{
    using entity = _Entity;
    using column = _Column;
    using table = typename column::table;
    using type = typename column::type;

    EntityValue(entity &entity)
        : m_entity(entity)
    {}

    void operator=(type val)
    {
        m_value = val;
        m_entity.changed[column::index] = true;
    }

    operator const type &() const
    {
        return m_value;
    }

    std::ostream &operator<<(std::ostream &stream)
    {
        stream << m_value;
        return stream;
    }

private:
    entity &m_entity;
    typename column::type m_value;
};

template<typename _TableImpl>
struct Table
{
    using impl = _TableImpl;

    //using Columns = _Columns;
};


}

namespace helpers {

}

#define QB_COLUMN(type, ...) helpers::buildColumnType<type, __VA_ARGS__>

#define EXPAND_COLUMNS(name, type, ...) \
    type name{u #name}; \
    EXPAND_COLUMNS(__VA_ARGS__)


#define QB_TABLE(tableName, ...) \
struct TableName : public Table<TableName, __VA_ARGS__> \
{ \
public: \
    static constexpr QStringView name = u #tableName; \
    EXPAND_COLUMNS(__VA_ARGS__) \
}; \

