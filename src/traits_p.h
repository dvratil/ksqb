/*
 * SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#pragma once

#include "schema.h"
#include "common.h"

#include <type_traits>
#include <sstream>

namespace Qb
{

namespace traits
{

///////////////////////////////////////////////////////////
// is_column

template<typename ... T>
struct is_column : std::false_type
{};

template<typename T, std::size_t I, typename Type>
struct is_column<Qb::Column<T, I, Type>> : std::true_type
{};

template<typename T, std::size_t I, typename Type>
struct is_column<const Qb::Column<T, I, Type>> : std::true_type
{};

template<typename ... T>
static constexpr bool is_column_v = is_column<T ...>::value;

///////////////////////////////////////////////////////////
// is_placeholder

template<typename T>
struct is_placeholder : std::conditional_t<std::is_base_of_v<Qb::Placeholder, T>, std::true_type, std::false_type>
{};

template<>
struct is_placeholder<Qb::Placeholder> : std::true_type
{};

template<typename T>
static constexpr bool is_placeholder_v = is_placeholder<T>::value;

///////////////////////////////////////////////////////////
// is_stream_serializable

template<typename T, typename = void>
struct is_stream_serializable : std::false_type
{};

template<typename T>
struct is_stream_serializable<T, std::void_t
    <decltype(std::declval<std::stringstream &>() << std::declval<T &&>())>> : std::true_type
{};

template<typename T>
static constexpr bool is_stream_serializable_v = is_stream_serializable<T>::value;


} // namespace traits
} // namespace Qb
