/*
 * SPDX-FileCopyrightText: 2020 Daniel Vrátil <dvratil@kde.org>
 *
 * SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#pragma once

#include "condition.h"
#include "common.h"
#include "stringhelpers_p.h"

#include <string>
#include <tuple>


namespace Qb
{

template<typename Column>
struct OrderByClause
{
    Column column = {};
    Order order = Order::ASC;
};


template<typename Column>
OrderByClause(Column &&, Order) -> OrderByClause<Column>;


template<typename _LimitExpr, typename _OffsetExpr = void *>
struct LimitClause
{
    using LimitExpr = _LimitExpr;
    using OffsetExpr = _OffsetExpr;

    static constexpr auto hasOffset = std::is_same_v<OffsetExpr, void *>;

    LimitExpr limitExpr = {};
    OffsetExpr offsetExpr = {};
};

template<typename LimitExpr>
LimitClause(LimitExpr &&) -> LimitClause<LimitExpr, void *>;
template<typename LimitExpr, typename OffsetExpr>
LimitClause(LimitExpr &&, OffsetExpr &&) -> LimitClause<LimitExpr, OffsetExpr>;

namespace detail
{
struct ColumnsPlaceholder : Placeholder {};
struct FromPlaceholder : Placeholder {};
struct WherePlaceholder : Placeholder {};
struct GroupByPlaceholder : Placeholder {};
struct HavingPlaceholder : Placeholder {};
struct OrderPlaceholder : Placeholder {};
struct LimitPlaceholder : Placeholder {};
} // namespace detail

template<typename _Columns = detail::ColumnsPlaceholder,
         typename _From = detail::FromPlaceholder,
         typename _Where = detail::WherePlaceholder,
         typename _GroupBy = detail::GroupByPlaceholder,
         typename _Having = detail::HavingPlaceholder,
         typename _Order = detail::OrderPlaceholder,
         typename _Limit = detail::LimitPlaceholder>
struct SelectQuery
{
public:
    using types = std::tuple<_Columns, _From, _Where, _GroupBy, _Having, _Order, _Limit>;

    template<typename Cols,
             typename From,
             typename Where,
             typename GroupBy,
             typename Having,
             typename Order,
             typename Limit>
    SelectQuery(Cols &&cols,
                From &&from,
                Where &&where,
                GroupBy &&groupBy,
                Having &&having,
                Order &&order,
                Limit &&limit)
        : m_columns(std::forward<Cols>(cols))
        , m_from(std::forward<From>(from))
        , m_where(std::forward<Where>(where))
        , m_groupBy(std::forward<GroupBy>(groupBy))
        , m_having(std::forward<Having>(having))
        , m_order(std::forward<Order>(order))
        , m_limit(std::forward<Limit>(limit))
    {}

    template<typename Cols>
    SelectQuery(Cols &&cols)
        : m_columns(std::forward<Cols>(cols))
    {}

    template<typename From>
    auto from() &&
    {
        return SelectQuery<_Columns, From, _Where, _GroupBy, _Having, _Order, _Limit>{
            std::move(m_columns), From{}, std::move(m_where), std::move(m_groupBy),
            std::move(m_having), std::move(m_order), std::move(m_limit)
        };
    }

    template<typename T>
    auto where(T &&cond) &&
    {
        return SelectQuery<_Columns, _From, T, _GroupBy, _Having, _Order, _Limit>{
                std::move(m_columns), std::move(m_from), std::move(cond), std::move(m_groupBy),
                std::move(m_having), std::move(m_order), std::move(m_limit)
        };
    }

    template<typename GroupByExpr>
    auto groupBy(GroupByExpr &&expr) &&
    {
        auto groupByStmts = appendStmtToTuple(std::move(m_groupBy), std::forward<GroupByExpr>(expr));
        return SelectQuery<_Columns, _From, _Where, decltype(groupByStmts), _Having, _Order, _Limit>{
                std::move(m_columns), std::move(m_from), std::move(m_where), std::move(groupByStmts),
                std::move(m_having), std::move(m_order), std::move(m_limit)
        };
    }

    template<typename HavingCond>
    auto having(HavingCond &&cond) &&
    {
        static_assert(!traits::is_placeholder_v<_GroupBy>, "HAVING clause can only be used with GROUP BY.");
        return SelectQuery<_Columns, _From, _Where, _GroupBy, HavingCond, _Order, _Limit>{
                std::move(m_columns), std::move(m_from), std::move(m_where), std::move(m_groupBy),
                std::move(cond), std::move(m_order), std::move(m_limit)
        };
    }


    template<typename Column>
    auto orderBy(const Column &col, Order order = Order::ASC)
    {
        auto orderStmts = appendStmtToTuple(std::move(m_order), OrderByClause{col, order});
        return SelectQuery<_Columns, _From, _Where, _GroupBy, _Having, decltype(orderStmts), _Limit>{
            std::move(m_columns), std::move(m_from), std::move(m_where), std::move(m_groupBy),
            std::move(m_having), std::move(orderStmts), std::move(m_limit)
        };
    }

    template<typename LimitExpr>
    auto limit(LimitExpr &&limitExpr)
    {
        static_assert(traits::is_placeholder_v<_Limit>, "LIMIT clause can only be specified once");
        LimitClause limitClause{std::forward<LimitExpr>(limitExpr)};
        return SelectQuery<_Columns, _From, _Where, _GroupBy, _Having, _Order, decltype(limitClause)>{
            std::move(m_columns), std::move(m_from), std::move(m_where), std::move(m_groupBy),
            std::move(m_having), std::move(m_order), std::move(limitClause)
        };
    }

    template<typename LimitExpr, typename OffsetExpr>
    auto limit(LimitExpr &&limitExpr, OffsetExpr &&offsetExpr)
    {
        static_assert(traits::is_placeholder_v<_Limit>, "LIMIT clause can only be specified once");
        LimitClause limitClause{std::forward<LimitExpr>(limitExpr), std::forward<OffsetExpr>(offsetExpr)};
        return SelectQuery<_Columns, _From, _Where, _GroupBy, _Having, _Order, decltype(limitClause)>{
            std::move(m_columns), std::move(m_from), std::move(m_where), std::move(m_groupBy),
            std::move(m_having), std::move(m_order), std::move(limitClause)
        };
    }

    std::string toString()
    {
        auto q = "SELECT " + columnsToString(m_columns) + " FROM " + tableToString(m_from);
        if constexpr (!traits::is_placeholder_v<_Where>) {
            q += " WHERE " + conditionToString(m_where);
        }

        if constexpr (!traits::is_placeholder_v<_GroupBy>) {
            q += " GROUP BY " + exprToString(m_groupBy);
        }

        if constexpr (!traits::is_placeholder_v<_Having>) {
            q += " HAVING " + conditionToString(m_having);
        }

        if constexpr (!traits::is_placeholder_v<_Order>) {
            q += " ORDER BY " + ordersToString(m_order);
        }

        if constexpr (!traits::is_placeholder_v<_Limit>) {
            q += " LIMIT " + exprToString(m_limit.limitExpr);
            if constexpr (_Limit::hasOffset) {
                q += " OFFSET " + exprToString(m_limit.offsetExpr);
            }
        }


        return q;
    }

private:
    template<typename Current, typename Append,
             std::enable_if_t<traits::is_placeholder_v<Current>, int> = 0>
    auto appendStmtToTuple(Current &&, Append &&append)
    {
        return std::make_tuple(std::forward<Append>(append));
    }

    template<typename Current, typename Append,
             std::enable_if_t<!traits::is_placeholder_v<Current>, int> = 0>
    auto appendOrderStmt(Current &&current, Append &&append)
    {
        return std::tuple_cat(std::forward<Current>(current), std::forward<Append>(append));
    }

private:
    _Columns m_columns;
    _From m_from;
    _Where m_where;
    _GroupBy m_groupBy;
    _Having m_having;
    _Order m_order;
    _Limit m_limit;
};

}; // namespace Qb

